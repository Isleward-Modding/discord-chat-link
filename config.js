define([

], function (

) {
	return {
		/**
		 * A Discord bot user token
		 */
		token: "YOUR TOKEN HERE",

		/**
		 * A Discord channel Snowflake (ID)
		 */
		channel: "CHANNEL ID HERE"
	};
});
