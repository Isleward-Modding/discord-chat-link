# Discord Chat Link Mod for Isleward
This mod displays in-game chat messages in a Discord channel and takes Discord messages and puts them in the in-game chat.

## Setup
Move this entire mod folder into `src/server/mods`.
Run `npm install` to install the dependencies.
Put your bot token and the Discord channel ID in `config.js`.
Run your Isleward server!
